package app.btssio.coffeeorderapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    Button buttonPlus, buttonMinus, buttonSubmitOrder;
    private CoffeeOrder anOrder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /**
         * Instanciation d'une commande de cafés
         */
        anOrder = new CoffeeOrder(2);

        /**
         * Association entre le bouton créé en Java et l'élément XML.
         * Gestion de l'événement onClick
         */
        buttonPlus = findViewById(R.id.button_plus);
        buttonPlus.setOnClickListener(new View.OnClickListener() {
            /**
             * Méthode appellée quand le bouton + est cliqué
             * @param view
             */
            @Override
            public void onClick(View view) {
                anOrder.addCoffee();
                int number = anOrder.getNumberOfCoffee();
                display(number);
            }
        });

        buttonMinus = findViewById(R.id.button_minus);
        buttonMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                anOrder.removeCoffee();
                int number = anOrder.getNumberOfCoffee();
                display(number);
            }
        });

        buttonSubmitOrder = findViewById(R.id.button_submit_order);
        buttonSubmitOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayPrice(anOrder.orderPrice());
            }
        });

    }

    /**
     * Méthode qui affiche à l'écran la quantité de la commande
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.text_view_number);
        quantityTextView.setText("" + number);
    }

    /**
     * Méthode qui affiche à l'écran le total de la commande
     */
    private void displayPrice(double number) {
        TextView priceTextView = (TextView) findViewById(R.id.text_view_result);
        priceTextView.setText(NumberFormat.getCurrencyInstance().format(number));
    }
}
